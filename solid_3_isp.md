# SOLID
**S**ingle Responsibility

**O**pen/Close

**L**iskov's Substitution

**I**nterface Segregation

**D**ependency Inversion.

## **I**nterface Segregation Principle (ISP)
### The Definition
  Clients should not be forced to depend on methods they do not use.

ISP acknowledges that there are objects that require noncohesive interfaces; however, it
suggests that clients should not know about them as a single class. Instead, clients should
know about abstract base classes that have cohesive interfaces.

When clients are forced to depend on methods they don't use, those clients are subject to
changes to those methods.


**Conclusion**
Fat classes cause bizarre and harmful couplings between their clients. When one client forces a
change on the fat class, all the other clients are affected. Thus, clients should have to
depend only on methods that they call. This can be achieved by breaking the interface of the
fat class into many client-specific interfaces.
Each client-specific interface declares only those functions that its particular client or
client group invoke. The fat class can then inherit all the client-specific interfaces and
implement them. This breaks the dependence of the clients on methods that they don't invoke
and allows the clients to be independent of one another.
