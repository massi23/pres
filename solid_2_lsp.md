# SOLID
**S**ingle Responsibility
**O**pen/Close
**L**iskov's Substitution
**I**nterface Segregation
**D**ependency Inversion.

## **L**iskov's Substitution Principle (LSP)
### The Definition
    Subtypes must be substitutable for their base types.
