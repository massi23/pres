# SOLID
**S**ingle Responsibility (SRP)
**O**pen/Close
**L**iskov's Substitution
**I**nterface Segregation
**D**ependency Inversion.

## **S**ingle Responsibility Principle
### The Definition
    A class should have only one reason to change (responsibility).

Defined by Robert C. Martin in his book Agile Software Development, Principles, Patterns, and Practices. What it states is very simple, however achieving that simplicity can be very tricky.

### The Audience
Determining the one single responsibility a class or module could be difficukt. Changes are usually requested by the users of the application or system we develop.


### Roles and Actors
Associating concrete persons to all of these roles may be difficult. In a small company a single person may need to satisfy several roles while in a large company the fire may be several persons allocated to a single role. So **it seems much more reasonable to think about the roles**.

_A responsibility is a family of functions that serves one particular actor. (Robert Martin)_

### Source of Change
In the sense of this reasoning, actors become a source of change for the family of functions that serves them. As their needs change, that specific family of functions must also change to accommodate their needs.

_An actor for a responsibility is the single source of change for that responsibility. (Robert C. Martin)_

_An axis of change is only an axis of change if the changes actually  occurr._

It is not wise to apply the SRP, or any other principle for that matter, if there is no symptom (**YAGNI**).

### Classic Examples
* Objects That Can "Print" Themselves
* Objects That Can "Save" Themselves

### Software Design Considerations
Many requirements affecting the same class may represent an axis of change that may be a clue for a single responsibility.
To achieve value of software functionality (satisfying as much requirements as possible) the software must be ease of change and to achieve this we've to respect SRP, so we easly change, extend and accommodate new functionalities.

_Why  was  it  important  to  separate  these  two  responsibilities  into  separate  classes?_

* **Because  each  responsibility  is  an  axis  of  change.**

* **When  the  requirements  change,  that change will be manifest through a change in responsibility amongst the classes.**

* **If a class assumes  more  than  one  responsibility,  then  there  will  be  more  than  one  reason  for  it  to change.**

If a class has more than one responsibility the responsibilities become coupled.
This  kind  of  coupling  leads  to  fragile  designs  that  break  in  unexpected  ways  when changed.


### Final Thoughts
The Single Responsibility Principle should always be considered when we write code. Class and module design is highly affected by it and it leads to a low coupled design with less and lighter dependencies. 
Excessive SRP consideration can easily lead to premature optimization and instead of a better design, it may lead to a scattered one where the clear responsibilities of classes or modules may be hard to understand. 

