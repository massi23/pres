** What Do I Tell My Manager? **

- If the manager is technically savvy, introducing the subject may not be that hard.
- If the manager is genuinely quality oriented, then the thing to stress is the quality aspects.
  Here using refactoring in the review process is a good way to work things.

Take a look at any book on reviews, inspections, or the software development process for the latest
citations. These should convince most managers of the value of reviews. It is then a short step to 
introduce refactoring as a way of getting review comments into the code.


	** Indirection and Refactoring **
	most refactoring introduces more indirection into a program =>  Refactoring tends to break big 
	objects into several smaller ones and big methods into several smaller ones.

	Every time you break one thing into two pieces, you have more things to manage. It also can make
	a program harder to read as an object delegates to an object delegating to an object. So you'd
	like to minimize indirection.

	However indirection can pay for itself:

	- To enable sharing of logic.
	- To explain intention and implementation separately.
	- To isolate change.
	- To encode conditional logic (polymorphism).

	a) Game -  Maintaining the current behavior of the system, how can you make your system more valuable,
		   either byincreasing its quality or by reducing its cost?

	 	   Identify a place where it is missing one or more of the benefits of indirection.
		   Now you have a more valuable program because it has more qualities that we will appreciate
		   tomorrow. The problem with this process is that it is too easy to guess wrong.

	b) Game -  Identify indirection that isn't paying for itself and take it out.





** Problem with refactoring **

???
-- DB --
One problem area for refactoring is databases. Most business applications are tightly coupled to
the database schema that supports them. That's one reason that the database is difficult to
change. Another reason is data migration. 

With nonobject databases a way to deal with this problem is to place a separate layer of software
between your object model and your database model. 

Such a layer adds complexity but gives you a lot of flexibility.
...


-- Changing Interfaces --
the interface is important: change that and anything can happen.
There is no problem changing a (public/protected) method name if you have access to all the code 
that calls that method. There is a problem only if the interface is being used by code that you
cannot find and change.  What do you do about refactorings that change published interfaces?
you have to retain both the old interface and the new one, at least until your users have had a
chance to react to the change. There is an alternative: Don't publish the interface :-)


-- Design Changes That Are Difficult to Refactor --
refactoring can be an alternative to upfront design. In this scenario you don't do any design
at all. You just code the first approach that comes into your head, get it working, and then
refactor it into shape.


With refactoring the emphasis changes. You still do upfront design, but now you don't try to find
the solution. Instead all you want is a reasonable solution. You know that as you build the
solution, as you understand more about the problem, you realize that the best solution is different
from the one you originally came up with. With refactoring this is not a problem, for it no longer is
expensive to make the changes.


The problem with building a flexible solution is that flexibility costs. Flexible
solutions are more complex than simple ones. The resulting software is more difficult to maintain
in general, although it is easier to flex in the direction I had in mind. 


With refactoring you approach the risks of change differently. You still think about potential
changes, you still consider flexible solutions. But instead of implementing these flexible solutions,
you ask yourself, "How difficult is it going to be to refactor a simple solution into the flexible
solution?" If, as happens most of the time, the answer is "pretty easy," then you just implement
the simple solution.


-- Refactoring and Performance --

A common concern with refactoring is the effect it has on the performance of a program. To make
the software easier to understand, you often make changes that will cause the program to run
more slowly. 

The secret to fast software, in all but hard real-time contexts, is to write tunable software
first and then to tune it for sufficient speed.


I've seen three general approaches to writing fast softwar

- budgeting
- constant attention
- delayed optimization









