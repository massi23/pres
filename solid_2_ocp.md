# SOLID
**S**ingle Responsibility
**O**pen/Close
**L**iskov's Substitution
**I**nterface Segregation
**D**ependency Inversion.

## Open/Closed Principle (OCP)

When a single change to a program results in a **cascade of changes** to dependent modules, the **design smells of rigidity**. OCP advises us to refactor the system so that further changes of that kind will not cause more modifications. If OCP is applied well, further changes of that kind are achieved by adding new code, not by changing old code that already works.

### Definition
    Software entities (classes, modules, functions, etc.) should be open for extension but closed for modification

### Description
Modules that conform to OCP have two primary attributes.
* **open for extension**
This means that **the behavior** of the module **can be extended**. As the requirements change, we can extend the module with **new behaviors that satisfy changes**.

* **closed for modification**
Extending the behavior of a module does not result in changes to the code of the module.



How is it possible that the behaviors of a module can be modified without changing its source code?
With **abstraction**. In any object-oriented programming language is possible to create abstractions that are fixed and yet represent an unbounded group of possible behaviors.

* the abstractions are abstract base classes
* the unbounded group of possible behaviors are represented by all the possible derivative classes

So a module that manipulates an abstraction is closed for modification (it depends on a fixed abstraction), but the behavior of that module can be extended by creating new derivatives of the abstraction.

### Anticipation and "Natural" Structure
No matter how "closed" a module is, there will always be some kind of change against which it is not closed. _There is no model that is natural to all *contexts*!_

Since closure cannot be complete, it must be strategic. The designer must choose the kinds of changes against which to close the design, must guess at the kinds of changes that are most likely, and then construct abstractions to protect against those changes.
This takes a certain amount of prescience derived from *experience*. Experienced designers hope that they know the users and the industry well enough to judge the probability of various kinds of changes. These designers then invoke OCP against the most probable changes.

Conforming to OCP is expensive. It takes time and effort to create the _appropriate_ abstractions. Those abstractions also increase the complexity of the software design.

We may permit ourselves to be fooled once. This means that we initially write the code expecting it not to change.
When a change occurs, we implement the *abstractions that protect us from future changes of that kind*.
Simulating changes:
* test
* short development cycles (days)
* develop by value ($)
* deploy early and often


### Stimulating change
We want to know what kinds of changes are likely before we are very far down the development path. The longer we wait to find out what kinds of changes are likely, the more difficult it will be to create the appropriate abstractions.

### considerazioni
quindi per rispetare OCP dobbiamo introdurre classi astratte e interfacce laddove vediamo possibili snodi o punti di cambiamento... questo puo' portare a scelte anticipatorie? a invesire tempo in design upfront? D'altro canto se non lo facessimo non rispetteremo OCP perché un cambio di requisiti si tradurrebbe come primo passo in una modifica al "modulo" corrente.
Credo che anche in questo caso pero' stiamo rispettando il principio, semplicemente lo facciamo in due passi:
* modifica al codice (refactoring) per introdurre le necessarie astrazioni
* implentazione dei nuovi requisiti con le relativi dipendenzte concrete
