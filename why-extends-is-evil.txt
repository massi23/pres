Why should you avoid implementation inheritance? 
The first problem is that explicit use of concrete class names locks you into specific implementations,
making down-the-line changes unnecessarily difficult.

Agile development methodologies: You start programming before you fully specify the program.
At the core of parallel development, however, is the notion of flexibility: You have to write
your code in such a way that you can incorporate newly discovered requirements into the existing
code as painlessly as possible.




** Coupling **
Non voglio l'accoppiamento perchè rende difficile e fragile il cambiamento: una modifica in un sistema
strettamente accoppiato potrebbe avere ripercussioni in molti punti.
(implementation hiding... cosa centra con extends is evil?)
-- low couplig => GRASP
-- protected variations => GRASP

 * The fragile base-class problem * => una modifica/introduzione di un comportamento in una classe base potrebbe avere conseguenze inaspettate su chi eredita
extends => tightly coupled

Note you don't have this problem if you use interface inheritance, since there's no inherited functionality to go bad on you. 
+ I still have the benefit of writing the equivalent of base-class code only once, because I use encapsulation rather than derivation
- riscrivo ogni volta che estendo anche qunado i comportamenti sono i medesimi


 * Frameworks => devo estendere per arrivare ad avere le funzionalità... approccio fragile: ogni volta che il framework cambia non posso garantire i miei comportamenti, anche se compila.
-- aggiungo: prototype programming? Due aspetti:
    - in un linguaggio fortemente tipato come java 


The more abstraction you add, the greater the flexibility. In today's business environment, where requirements regularly change as the program develops, this flexibility is essential. 
Moreover, most of the Agile development methodologies (such as Crystal and extreme programming) simply won't work unless the code is written in the abstract.


